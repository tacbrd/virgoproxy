/*
* Copyright (c) 1998 - 2014. OptiMetrics, Inc. All Rights Reserved.
*
* U.S. Government End Users and their Contractors: This software is provided
* to the U.S. Government with "Unlimited Rights" as that term is defined at
* DFARS 252-227.7014.
*
* All Other Users: License rights, including the rights to use and copy the
* source code, are strictly limited to those specific rights enumerated in the
* License Agreement under which this software was provided.
* --------------------------------------
* WARNING-This computer program and its documentation contains technical data
* whose export is restricted by the Arms Export Control Act (Title 22 U.S.C.
* Sec. 2751 et seq.) or Executive Order 12470. Violators of these export laws
* are subject to severe criminal penalties.
* --------------------------------------
* IMPORTANT NOTICE: A portion of the source code contained in this software
* product was developed by OptiMetrics, Inc. at its own private expense; the
* remainder was developed by OptiMetrics, Inc. under contract to the U.S.
* Government. As sole developer, OptiMetrics, Inc. owns this software source
* code in its entirety and reserves all rights of ownership to itself. IN NO
* EVENT MAY THE SOURCE CODE CONTAINED IN THIS MODULE BE USED IN ANY WAY BY ANY
* THIRD PARTY FOR ANY COMMERCIAL (NON-U.S. GOVERNMENT) PURPOSE WITHOUT THE
* EXPRESS WRITTEN CONSENT OF OPTIMETRICS, INC.
*
* OptiMetrics, Inc.
* 100 Walter Ward Boulevard
* Abingdon, Maryland 21009
* 410.417.5780
*/
package org.tacbrd.proxy.virgo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

public class DelegateServlet extends HttpServlet {

	private static final long serialVersionUID = -7826009351912558375L;
    private static final Logger logger = LoggerFactory.getLogger(DelegateServlet.class);
    private String VIRGO_URL = "http://localhost";
    private static String PROP_VIRGO_URL = "org.tacbrd.virgo.url";
    
    public DelegateServlet() {
    	try {
			VIRGO_URL = PrefsPropsUtil.getString(PROP_VIRGO_URL, VIRGO_URL);
		} catch (SystemException e) {
			logger.error("DelegateServlet could not find " + PROP_VIRGO_URL + ": " + e.getMessage());
		}
    }

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	@Override
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	@Override
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}


	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		try {
			// authorize
			if(! isAuthorized(request)) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			// get the url we are rewriting to
			String newUrl = getRedirectUrl(request);
			URL url = new URL(newUrl);
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse res;

			if(request.getMethod().equals("GET")) {
				// GET
				HttpGet req = new HttpGet();
				req.setURI(url.toURI());
				// pass the headers we need
				handleHeaders(request,req);
				res = client.execute(req);
				String out = getStringFromInputStream(res.getEntity().getContent());
				response.setStatus(res.getStatusLine().getStatusCode());
				response.getOutputStream().write(out.getBytes());
			} else if(request.getMethod().equals("POST")) {
				// POST
				HttpPost req = new HttpPost();
				req.setURI(url.toURI());
				String body = getStringFromInputStream(request.getInputStream());
				StringEntity input = new StringEntity(body);
				req.setEntity(input);
				// pass the headers we need
				handleHeaders(request,req);
				res = client.execute(req);
				String out = getStringFromInputStream(res.getEntity().getContent());
				response.setStatus(res.getStatusLine().getStatusCode());
				response.getOutputStream().write(out.getBytes());
			} else if(request.getMethod().equals("DELETE")) {
				// DELETE
				HttpDelete req = new HttpDelete();
				req.setURI(url.toURI());
				res = client.execute(req);
				String out = getStringFromInputStream(res.getEntity().getContent());
				response.setStatus(res.getStatusLine().getStatusCode());
				response.getOutputStream().write(out.getBytes());
			} else if(request.getMethod().equals("PUT")) {
				// PUT
				HttpPut req = new HttpPut();
				req.setURI(url.toURI());
				String body = getStringFromInputStream(request.getInputStream());
				StringEntity input = new StringEntity(body);
				req.setEntity(input);
				// pass the headers we need
				handleHeaders(request,req);
				res = client.execute(req);
				String out = getStringFromInputStream(res.getEntity().getContent());
				response.setStatus(res.getStatusLine().getStatusCode());
				response.getOutputStream().write(out.getBytes());
			}
			// TODO: is there an else case here?
		} catch (Exception e) {
			logger.error("Proxy connection error: " + e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	private String getRedirectUrl(HttpServletRequest request) {
		// get the url we are rewriting to
		String reqUrl = request.getRequestURI();
		String[] parts = reqUrl.split("/VirgoProxy/");			
		// TODO: make this a server parameter
		String newUrl = VIRGO_URL + "/" + parts[1];
		String query = request.getQueryString();
		if(query!=null) {
			newUrl += "?" + query;
		}
		return newUrl;
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "VirgoProxy";
	}
	
	private static String getStringFromInputStream(InputStream is) { 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder(); 
		String line;
		try { 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} 
		return sb.toString(); 
	}
	
	private static boolean isAuthorized(HttpServletRequest request) throws PortalException, SystemException {
		User user = PortalUtil.getUser(request);
		if(user != null) {
			return true;
		} 
		return false;
	}
	
	private static void handleHeaders(HttpServletRequest request, HttpRequestBase req) {
		// pass the headers we need
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			if(key.equals("accept")) 
				req.addHeader(key, value);
		}		
	}
}
