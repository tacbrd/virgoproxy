# Virgo Proxy #

A proxy servlet that plugs in to liferay and authenticates rest calls before passing them to a configured virgo server.

### Configuration ###

In the portal-ext.properties set the base url for the Virgo server.

```
#!java

org.tacbrd.virgo.url=http://localhost/
```


### Usage ###

If the virgo url is 
	
```
#!java

http://localhost/enterprise/map/rest/maps
```

the authenticated liferay url will be 
	
```
#!java

http://localhost:8080/delegate/VirgoProxy/enterprise/map/rest/maps
```
